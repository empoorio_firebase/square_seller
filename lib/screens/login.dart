import 'dart:io';

import 'package:active_ecommerce_seller_app/app_config.dart';
import 'package:active_ecommerce_seller_app/custom/input_decorations.dart';
import 'package:active_ecommerce_seller_app/custom/intl_phone_input.dart';
import 'package:active_ecommerce_seller_app/custom/localization.dart';
import 'package:active_ecommerce_seller_app/custom/my_widget.dart';
import 'package:active_ecommerce_seller_app/custom/toast_component.dart';
import 'package:active_ecommerce_seller_app/helpers/app_helper.dart';
import 'package:active_ecommerce_seller_app/helpers/auth_helper.dart';
import 'package:active_ecommerce_seller_app/helpers/shared_value_helper.dart';
import 'package:active_ecommerce_seller_app/my_theme.dart';
import 'package:active_ecommerce_seller_app/repositories/auth_repository.dart';
import 'package:active_ecommerce_seller_app/screens/authScreen.dart';
import 'package:active_ecommerce_seller_app/screens/home.dart';
import 'package:active_ecommerce_seller_app/screens/main.dart';
import 'package:active_ecommerce_seller_app/screens/password_forget.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:toast/toast.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../widgets/appAppBar.dart';
import '../widgets/appButton.dart';
import '../widgets/appTextFormField.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String _login_by = "email"; //phone or email
  String initialCountry = 'US';
  PhoneNumber phoneCode = PhoneNumber(isoCode: 'US', dialCode: "+1");
  String _phone = "";
  BuildContext loadingContext;

  //controllers
  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  MyWidget myWidget;

  @override
  void initState() {
    //on Splash Screen hide statusbar
    //SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    super.initState();
    /*if (is_logged_in.value == true) {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return Main();
      }));
    }*/
  }

  @override
  void dispose() {
    //before going to other screen show statusbar
    //SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top, SystemUiOverlay.bottom]);
    super.dispose();
  }

  onPressedLogin() async {
    var email = _emailController.text.toString();
    var password = _passwordController.text.toString();

    if (_login_by == 'email' && email == "") {
      ToastComponent.showDialog("Enter email", gravity: Toast.center, duration: Toast.lengthLong);
      return;
    } else if (_login_by == 'phone' && _phone == "") {
      ToastComponent.showDialog("Enter phone number",
          gravity: Toast.center, duration: Toast.lengthLong);
      return;
    } else if (password == "") {
      ToastComponent.showDialog("Enter password",
          gravity: Toast.center, duration: Toast.lengthLong);
      return;
    }
    loading();
    var loginResponse =
        await AuthRepository().getLoginResponse(_login_by == 'email' ? email : _phone, password);
    Navigator.pop(loadingContext);

    if (loginResponse.result == false) {
      ToastComponent.showDialog(loginResponse.message,
          gravity: Toast.center, duration: Toast.lengthLong);
    } else {
      ToastComponent.showDialog(loginResponse.message,
          gravity: Toast.center, duration: Toast.lengthLong);
      AuthHelper().setUserData(loginResponse);

      access_token.load().whenComplete(() {
        if (access_token.$.isNotEmpty) {
          print(access_token.$);

          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
            builder: (context) {
              return Main();
            },
          ), (route) => false);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    //myWidget = MyWidget(myContext: context);
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle:
            isDarkMode(context) ? SystemUiOverlayStyle.light : SystemUiOverlayStyle.dark,
        leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => AuthScreen(),
              ));
            },
            icon: Icon(
              Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
              color: !isDarkMode(context) ? Colors.black : Colors.white,
            )),
        elevation: 0,
        backgroundColor: isDarkMode(context) ? Colors.black : Colors.white,
      ),
      extendBodyBehindAppBar: true,
      backgroundColor: isDarkMode(context) ? Colors.black : Colors.white,
      body: buildBody(context),
    );
  }

  buildBody(context) {
    final _screen_width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(child: SizedBox()),
            // Container(
            //   width: _screen_width / 2,
            //   child: Text(
            //     AppConfig.app_name,
            //     style:
            //         TextStyle(color: Colors.white, fontSize: 22, fontWeight: FontWeight.w500),
            //   ),
            // ),
            // Padding(
            //   padding: const EdgeInsets.only(
            //     top: 40,
            //     bottom: 30.0,
            //   ),
            //   child: Text(
            //     LangText(context: context).getLocal().login_screen_login_text,
            //     style: TextStyle(
            //         color: MyTheme.app_accent_border, fontSize: 20, fontWeight: FontWeight.w300),
            //   ),
            // ),
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Text(
                'sign In',
                style: TextStyle(
                    color: !isDarkMode(context) ? Colors.black : Colors.white,
                    fontSize: 45.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            // login form container
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: Text(
                    _login_by == "email" ? "Email" : "Phone",
                    style: const TextStyle(
                        color: MyTheme.app_accent_border,
                        fontWeight: FontWeight.w400,
                        fontSize: 12),
                  ),
                ),
                //  if (_login_by == "email")
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      AppTextFormField(
                        controller: _emailController,
                        cursorColor: !isDarkMode(context) ? Colors.black : Colors.white,
                        focusColors: !isDarkMode(context) ? Colors.black : Colors.white,
                        hintext: "seller@example.com",
                      ),
                    ],
                  ),
                ),
                /* else
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Color.fromRGBO(255, 255, 255, 0.5)),
                          height: 36,
                          child: CustomInternationalPhoneNumberInput(
                            onInputChanged: (PhoneNumber number) {
                              print(number.phoneNumber);
                              setState(() {
                                _phone = number.phoneNumber;
                              });
                            },
                            onInputValidated: (bool value) {
                              print('on input validation ${value}');
                            },
                            selectorConfig: SelectorConfig(
                              selectorType: PhoneInputSelectorType.DIALOG,
                            ),
                            ignoreBlank: false,
                            autoValidateMode: AutovalidateMode.disabled,
                            selectorTextStyle:
                                TextStyle(color: MyTheme.font_grey),
                            textStyle: TextStyle(color: Colors.white54),
                            initialValue: phoneCode,
                            textFieldController: _phoneNumberController,
                            formatInput: true,
                            keyboardType: TextInputType.numberWithOptions(
                                signed: true, decimal: true),
                            inputDecoration:
                                InputDecorations.buildInputDecoration_phone(
                                    hint_text: "01710 333 558"),
                            onSaved: (PhoneNumber number) {
                              print('On Saved: $number');
                            },
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              _login_by = "email";
                            });
                          },
                          child: Text(
                            "or, Login with an email",
                            style: TextStyle(
                                color: MyTheme.app_accent_color,
                                fontStyle: FontStyle.italic,
                                decoration: TextDecoration.underline),
                          ),
                        )
                      ],
                    ),
                  ),*/
                Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: Text(
                    "Password",
                    style: TextStyle(
                        color: MyTheme.app_accent_border,
                        fontWeight: FontWeight.w400,
                        fontSize: 12),
                  ),
                ),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    AppTextFormField(
                        controller: _passwordController,
                        cursorColor: !isDarkMode(context) ? Colors.black : Colors.white,
                        focusColors: !isDarkMode(context) ? Colors.black : Colors.white,
                        obscureText: true),
                    // Container(
                    //   decoration: BoxDecoration(
                    //       borderRadius: BorderRadius.circular(10),
                    //       color: Color.fromRGBO(255, 255, 255, 0.5)),
                    //   height: 36,
                    //   child: TextField(
                    //     controller: _passwordController,
                    //     autofocus: false,
                    //     obscureText: true,
                    //     enableSuggestions: false,
                    //     autocorrect: false,
                    //     style: TextStyle(color: MyTheme.white),
                    //     decoration: InputDecorations.buildInputDecoration_1(
                    //         borderColor: MyTheme.noColor,
                    //         hint_text: "• • • • • • • •",
                    //         hintTextColor: MyTheme.dark_grey),
                    //   ),
                    // ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) {
                            return PasswordForget();
                          }));
                        },
                        child: Text(
                          "Forgot Password?",
                          style: TextStyle(
                              color: !isDarkMode(context) ? Colors.black : Colors.white,
                              decoration: TextDecoration.underline),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
            Expanded(child: SizedBox()),
            AppButton(
              title: 'Login',
              backgroundColor: !isDarkMode(context) ? Colors.black : Colors.white,
              titleColor: isDarkMode(context) ? Colors.black : Colors.white,
              onTap: () {
                onPressedLogin();
              },
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 20,
              ),
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  LangText(context: context).getLocal().login_screen_login_alert,
                  style: TextStyle(fontSize: 12, color: MyTheme.app_accent_border),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  loading() {
    showDialog(
        context: context,
        builder: (context) {
          loadingContext = context;
          return AlertDialog(
              content: Row(
            children: [
              CircularProgressIndicator(),
              SizedBox(
                width: 10,
              ),
              Text("${AppLocalizations.of(context).common_loading}"),
            ],
          ));
        });
  }
}

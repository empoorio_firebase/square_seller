import 'package:active_ecommerce_seller_app/screens/login.dart';
import 'package:active_ecommerce_seller_app/widgets/appButton.dart';
import 'package:active_ecommerce_seller_app/widgets/orDivider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AuthScreen extends StatelessWidget {
  const AuthScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        // systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/pexels-tembela-bohle-1884579.jpg'),
                    fit: BoxFit.cover,
                    opacity: 0.6)),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(child: SizedBox()),
                Image.asset(
                  'assets/logo/white_logo.png',
                  height: 170,
                ),
                Expanded(child: SizedBox()),
                AppButton(
                  title: 'Login',
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Login(),
                        ));
                  },
                ),
                // SizedBox(
                //   height: 10,
                // ),
                // AppButton(
                //   imagePath: 'assets/icon/apple_logo_white.svg.png',
                //   title: 'Continue with Apple',
                //   onTap: () {
                //     // Navigator.pushReplacement(
                //     //     context,
                //     //     MaterialPageRoute(
                //     //       builder: (context) => Login(),
                //     //     ));
                //   },
                // ),
                // SizedBox(
                //   height: 10,
                // ),
                // AppButton(
                //   imagePath:
                //       'assets/icon/google-logo-png-google-icon-logo-png-transparent-svg-vector-bie-supply-14.png',
                //   title: 'Continue with google',
                //   onTap: () {
                //     // Navigator.pushReplacement(
                //     //     context,
                //     //     MaterialPageRoute(
                //     //       builder: (context) => Login(),
                //     //     ));
                //   },
                // ),
                // SizedBox(
                //   height: 10,
                // ),
                // AppButton(
                //   imagePath: 'assets/icon/MetaMask_Fox.svg.png',
                //   backgroundColor: Colors.black.withOpacity(0.6),
                //   title: 'Continue with Metamask',
                //   onTap: () {
                //     // Navigator.pushReplacement(
                //     //     context,
                //     //     MaterialPageRoute(
                //     //       builder: (context) => Login(),
                //     //     ));
                //   },
                // ),
                // SizedBox(
                //   height: 20,
                // ),
                // OrDivider(
                //   text: 'Or',
                // ),
                // SizedBox(
                //   height: 20,
                // ),
                // AppButton(
                //   borderColor: Colors.white,
                //   backgroundColor: Colors.transparent,
                //   borderWidth: 1.0,
                //   title: 'Sign up',
                //   onTap: () {
                //     // Navigator.pushReplacement(
                //     //     context,
                //     //     MaterialPageRoute(
                //     //       builder: (context) => Login(),
                //     //     ));
                //   },
                // ),
                SizedBox(
                  height: 30,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

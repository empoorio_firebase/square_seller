import 'dart:io';

import 'package:active_ecommerce_seller_app/custom/input_decorations.dart';
import 'package:active_ecommerce_seller_app/custom/localization.dart';
import 'package:active_ecommerce_seller_app/custom/toast_component.dart';
import 'package:active_ecommerce_seller_app/helpers/app_helper.dart';
import 'package:active_ecommerce_seller_app/helpers/shared_value_helper.dart';
import 'package:active_ecommerce_seller_app/my_theme.dart';
import 'package:active_ecommerce_seller_app/repositories/auth_repository.dart';
import 'package:active_ecommerce_seller_app/screens/login.dart';
import 'package:active_ecommerce_seller_app/screens/password_otp.dart';
import 'package:active_ecommerce_seller_app/widgets/appButton.dart';
import 'package:active_ecommerce_seller_app/widgets/appTextFormField.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:toast/toast.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PasswordForget extends StatefulWidget {
  @override
  _PasswordForgetState createState() => _PasswordForgetState();
}

class _PasswordForgetState extends State<PasswordForget> {
  String _send_code_by = "email"; //phone or email
  String initialCountry = 'US';
  PhoneNumber phoneCode = PhoneNumber(isoCode: 'US');
  String _phone = "";

  //controllers
  TextEditingController _emailController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();

  @override
  void initState() {
    //on Splash Screen hide statusbar
    // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    super.initState();
  }

  @override
  void dispose() {
    //before going to other screen show statusbar
    // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top, SystemUiOverlay.bottom]);
    super.dispose();
  }

  onPressSendCode() async {
    var email = _emailController.text.toString();

    if (_send_code_by == 'email' && email == "") {
      ToastComponent.showDialog(AppLocalizations.of(context).password_forget_screen_email_warning,
          gravity: Toast.center, duration: Toast.lengthLong);
      return;
    }
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return PasswordOtp(
        verify_by: _send_code_by,
        email: email,
      );
    }));
    var passwordForgetResponse = await AuthRepository()
        .getPasswordForgetResponse(_send_code_by == 'email' ? email : _phone, _send_code_by);

    if (passwordForgetResponse.result == false) {
      ToastComponent.showDialog(passwordForgetResponse.message,
          gravity: Toast.center, duration: Toast.lengthLong);
    } else {
      ToastComponent.showDialog(passwordForgetResponse.message,
          gravity: Toast.center, duration: Toast.lengthLong);

      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return PasswordOtp(
          verify_by: _send_code_by,
          email: email,
        );
      }));
    }
  }

  @override
  Widget build(BuildContext context) {
    final _screen_height = MediaQuery.of(context).size.height;
    final _screen_width = MediaQuery.of(context).size.width;
    return Directionality(
      textDirection: app_language_rtl.$ ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          systemOverlayStyle:
              isDarkMode(context) ? SystemUiOverlayStyle.light : SystemUiOverlayStyle.dark,
          leading: IconButton(
              onPressed: () {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => Login(),
                ));
              },
              icon: Icon(
                Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                color: !isDarkMode(context) ? Colors.black : Colors.white,
              )),
          elevation: 0,
          backgroundColor: isDarkMode(context) ? Colors.black : Colors.white,
        ),
        backgroundColor: !isDarkMode(context) ? Colors.white : Colors.black,
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          width: double.infinity,
          child: Column(
            children: [
              Expanded(
                child: SizedBox(),
              ),
              Row(
                children: [
                  Text(
                    "Forget Password ?",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        color: isDarkMode(context) ? Colors.white : Colors.black,
                        fontSize: 25,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 4.0),
                child: Text(
                  AppLocalizations.of(context).password_forget_screen_email,
                  style: TextStyle(color: MyTheme.white, fontWeight: FontWeight.w600),
                ),
              ),

              AppTextFormField(
                cursorColor: !isDarkMode(context) ? Colors.black : Colors.white,
                focusColors: !isDarkMode(context) ? Colors.black : Colors.white,
                controller: _emailController,
                hintext: "johndoe@example.com",
              ),
              SizedBox(
                height: 10,
              ),
              // TextField(
              //   controller: _emailController,
              //   autofocus: false,
              //   decoration: InputDecorations.buildInputDecoration_1(
              //       hint_text: "johndoe@example.com",
              //       hintTextColor: MyTheme.dark_grey,
              //       borderColor: MyTheme.noColor),
              // ),
              Text(
                LangText(context: context).getLocal().password_forget_screen_info,
                style: TextStyle(
                  fontSize: 12,
                  color: MyTheme.grey_153,
                ),
                textHeightBehavior: TextHeightBehavior(applyHeightToFirstAscent: false),
                softWrap: false,
              ),

              Expanded(
                child: SizedBox(),
              ),

              AppButton(
                backgroundColor: !isDarkMode(context) ? Colors.black : Colors.white,
                titleColor: isDarkMode(context) ? Colors.black : Colors.white,
                title: 'Send Code',
                onTap: () {
                  onPressSendCode();
                },
              ),
              SizedBox(
                height: 30,
              )
              // Padding(
              //   padding: const EdgeInsets.only(top: 40.0),
              //   child: Container(
              //     height: 45,
              //     width: MediaQuery.of(context).size.width,
              //     decoration: BoxDecoration(
              //         border: Border.all(color: MyTheme.textfield_grey, width: 1),
              //         borderRadius: const BorderRadius.all(Radius.circular(12.0))),
              //     child: TextButton(
              //       style: ButtonStyle(
              //           backgroundColor: MaterialStateProperty.all(MyTheme.white),
              //           shape: MaterialStateProperty.all(
              //             RoundedRectangleBorder(
              //                 borderRadius: const BorderRadius.all(Radius.circular(12.0))),
              //           )),
              //       //height: 50,

              //       child: Text(
              //         "Send Code",
              //         style: TextStyle(
              //             color: MyTheme.app_accent_color,
              //             fontSize: 14,
              //             fontWeight: FontWeight.w600),
              //       ),
              //       onPressed: () {
              //         onPressSendCode();
              //       },
              //     ),
              //   ),
              // )
            ],
          ),
        ),
      ),
    );
  }
}

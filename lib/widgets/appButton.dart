import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  String title;
  String imagePath;
  Color backgroundColor;
  Color titleColor;
  double borderWidth;
  Color borderColor;
  Function onTap;
  String imageAsset;

  AppButton(
      {Key key,
      this.title = '',
      this.imagePath = '',
      this.backgroundColor,
      this.titleColor,
      this.borderWidth,
      this.borderColor,
      this.onTap,
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
        constraints: BoxConstraints(minWidth: double.infinity),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: backgroundColor ?? Colors.black,
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 14),
                elevation: 0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side: BorderSide(
                        width: borderWidth ?? 0, color: borderColor ?? Colors.transparent))),
            onPressed: () {
              onTap();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (imagePath != '') Image.asset(imagePath, height: 16,),
                if (imagePath != '')
                  SizedBox(
                    width: 6,
                  ),
                Text(
                  title,
                  style: TextStyle(fontSize: 16, color: titleColor ?? Colors.white),
                ),
              ],
            )));
  }
}

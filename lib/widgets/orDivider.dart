import 'package:flutter/material.dart';

class OrDivider extends StatelessWidget {
  String text;
  OrDivider({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
              child: Divider(
            color: Colors.grey.shade200,
          )),
          SizedBox(
            width: 10,
          ),
          Text(
            text,
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
              child: Divider(
            color: Colors.grey.shade200,
          )),
        ],
      ),
    );
  }
}

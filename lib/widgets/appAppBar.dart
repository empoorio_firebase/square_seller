import 'package:active_ecommerce_seller_app/helpers/app_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppAppbar extends StatelessWidget with PreferredSizeWidget {
  const AppAppbar({Key key}) : super(key: key);

  @override
  // TODO: implement preferredSize
  @override
  Size get preferredSize => Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.red,
      title: Text('algo'),
      systemOverlayStyle: SystemUiOverlayStyle.dark,
    );
  }
}

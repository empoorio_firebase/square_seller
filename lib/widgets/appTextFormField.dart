import 'package:flutter/material.dart';

import '../helpers/app_helper.dart';

class AppTextFormField extends StatelessWidget {
  Color cursorColor;
  TextEditingController controller;
  String hintext;
  Color errorColor;
  Color focusColors;
  bool isDense;
  bool obscureText;

  AppTextFormField(
      {Key key,
      this.cursorColor,
      this.controller,
      this.hintext = '',
      this.errorColor,
      this.isDense = false,
      this.focusColors,
      this.obscureText})
      : super(key: key);

  BorderRadius borderRadius = BorderRadius.circular(8.0);
  @override
  Widget build(BuildContext context) {
    return TextFormField(
        obscureText: obscureText ?? false,
        
        controller: controller,
        cursorColor: cursorColor ?? Colors.black,
        decoration: InputDecoration(
          isDense: isDense,
          contentPadding: EdgeInsets.only(left: 16, right: 16),
          hintText: hintext,
          focusedBorder: OutlineInputBorder(
          
              borderRadius: borderRadius,
              borderSide: BorderSide(color: focusColors ?? Colors.black, width: 2.0)),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: errorColor ?? Colors.red),
            borderRadius: borderRadius,
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: errorColor ?? Colors.red),
            borderRadius: borderRadius,
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade200),
            borderRadius: borderRadius,
          ),
        ));
  }
}

var this_year = DateTime.now().year.toString();

class AppConfig {
  static String copyright_text = "@ GnoosiS Inc. " + this_year; //this shows in the splash screen
  static String app_name = "Square Sellers"; //this shows in the splash screen
  static String purchase_code =
      "05a27248-23df-41cb-b657-41f5c53faaa9"; //enter your purchase code for the app from codecanyon
  //static String purchase_code = ""; //enter your purchase code for the app from codecanyon

  //configure this
  static const bool HTTPS = false;

  //

  //

  //configure this
static const DOMAIN_PATH = "3.83.214.18"; //inside a folder
  //static const DOMAIN_PATH = "mydomain.com"; // directly inside the public folder

  //do not configure these below
  static const String API_ENDPATH = "api/v2";
  static const String PUBLIC_FOLDER = "public";
  static const String PROTOCOL = HTTPS ? "https://" : "http://";
  static const String SELLER_PREFIX = "seller";
  static const String RAW_BASE_URL = "${PROTOCOL}${DOMAIN_PATH}";
  static const String BASE_URL = "${RAW_BASE_URL}/${API_ENDPATH}";
  static const String BASE_URL_WITH_PREFIX = "${BASE_URL}/${SELLER_PREFIX}";
}
